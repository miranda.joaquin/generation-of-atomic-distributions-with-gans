#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 18 06:37:31 2022

@author: joaquin
"""

import os, sys
import imageio as iio
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageOps

from tensorflow import keras
import tensorflow as tf
from sklearn.model_selection import train_test_split

from tensorflow.keras.layers import Conv2DTranspose
from tqdm import tqdm

############### Preprocessing Images ################3
##### Remove white edges and Jmol logo  ###########
file_src0="snapshots_trimmed/"
for x in os.listdir(file_src0):
    img = Image.open(os.path.join(file_src0, x))
    cropped = img.crop((4.4, 4.4, 516.2, 516.2))  
    #     cropped = img.crop((4.75, 4.75, 258.75, 258.75)) 
    cropped.save(os.path.join(file_src0, x))

######## rotate 180 grades  $##############
Double the amount of data by rotating 180 every snapshot
file_src2="snapshots_trimmed"
file_src3="snapshots_rotate180"
for x in os.listdir(file_src2):
    img = Image.open(os.path.join(file_src2, x))
    rotate_img = img.rotate(180)  
    rotate_img.save(os.path.join(file_src3, x))

# size = 64, 64         
# file_src2="snapshots_256x256"
# file_src3="snapshots_64x64"
# for x in os.listdir(file_src2):
#     img = Image.open(os.path.join(file_src2, x))
#     img.thumbnail(size, Image.ANTIALIAS)
#     img.save(os.path.join(file_src3, x))

# snaps_train = []          # list with images
# temperatures_train = []   # list temperatures
# y_train = []              # list energies eV
# snaps_test = []          # list with images
# temperatures_test = []   # list temperatures
# y_test = []              # list energies eV

real_images_all = []    # list with images
energies  = []          # list energies eV
count_img = 0

# Read all the image files from folder snapshots. The files names have
# the values of temperature, composition and energy. Split values, get them
#  and fill lists with  images and values.

file_src="snapshots_512x512/"
for x in os.listdir(file_src):
#        print(os.path.basename(x))
        energy =' _'.join(x.split('_')[3:4])
        # print(energy)
        energies.append(energy)
        snap = iio.imread(file_src + x)
        real_images_all.append(snap)

del snap, energy

for i in range(4):
    plt.subplot(2, 2, i+1)
    plt.imshow(real_images_all[i])
    plt.axis('off')
    pair = (i, energies[i] )
    plt.title(pair, fontdict={'size': 8})
plt.show()

print( np.shape(real_images_all) )
real_images_all= np.array(real_images_all).astype('float32')/512
#real_images_all = real_images_all.reshape(-1, 64, 64, 1)
print( np.shape(real_images_all) )

# Initialisierung des Zufallszahlengenerators
np.random.seed(0)
batch_size = 5
all_images = 102

# Anzahl der Zufallszahlen zur Erzeugung eines Bildes
z_size = 5

# shapeimage
# in_shape=(254, 254, 3)
# before input_shape=(z_size,)

# Optimizer festlegen
optimizer = keras.optimizers.RMSprop(lr=0.0008, clipvalue=1.0, decay=1e-8)

# dropout ist wichtig bei GAN
dropout_rate = 0.2


# die Steigung der LeakyReLu im negativen Bereich
leaky_faktor = 0.2
# Neuronales Netz für Generator
generator = keras.models.Sequential()

generator.add(keras.layers.Dense(2*2*128, input_shape=(z_size,), name='dense_generator_1a'))
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Dense(2*2*256, input_shape=(z_size,), name='dense_generator_1b'))
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Dense(2*2*512, input_shape=(z_size,), name='dense_generator_1c'))
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Dense(2*2*1024, input_shape=(z_size,), name='dense_generator_1d'))
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Dense(2*2*1024, input_shape=(z_size,), name='dense_generator_1e'))
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Reshape((2, 2, 1024)))

generator.add(keras.layers.Conv2DTranspose(32, (3, 3), strides=(2, 2), padding='same', name='conv2D_generator_1c'))
generator.add(keras.layers.BatchNormalization())
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Conv2DTranspose(32, (3, 3), strides=(2, 2), padding='same', name='conv2D_generator_1d'))
generator.add(keras.layers.BatchNormalization())
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Conv2DTranspose(32, (3, 3), strides=(2, 2), padding='same', name='conv2D_generator_1e'))
generator.add(keras.layers.BatchNormalization())
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Conv2DTranspose(32, (3, 3), strides=(2, 2), padding='same', name='conv2D_generator_1f'))
generator.add(keras.layers.BatchNormalization())
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Conv2DTranspose(16, (3, 3), strides=(2, 2), padding='same', name='conv2D_generator_2'))
generator.add(keras.layers.BatchNormalization())
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Conv2DTranspose(8, (5, 5), strides=(2, 2), padding='same', name='conv2D_generator_3'))
generator.add(keras.layers.BatchNormalization())
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Conv2DTranspose(8, (5, 5), strides=(2, 2), padding='same', name='conv2D_generator_4'))
generator.add(keras.layers.BatchNormalization())
generator.add(keras.layers.LeakyReLU())

generator.add(keras.layers.Conv2DTranspose(3, (5, 5), strides=(2, 2), padding='same',
                                           activation='sigmoid', name='conv2D_generator_5'))

generator.compile(loss='binary_crossentropy', optimizer=optimizer)
generator.summary()


# Neuronales Netz für Deskriminator
discriminator = keras.models.Sequential()
discriminator.add(keras.layers.Conv2D(8, (5, 5), padding='same', activation='relu',
                                      input_shape=(512, 512, 3), name='conv2D_discriminator_1'))
discriminator.add(keras.layers.MaxPool2D(pool_size=(2, 2), padding='same'))
discriminator.add(keras.layers.Conv2D(16, (5, 5), padding='same', activation='relu', name='conv2D_discriminator_2'))
discriminator.add(keras.layers.MaxPool2D(pool_size=(2, 2), padding='same'))
discriminator.add(keras.layers.Conv2D(32, (3, 3), padding='same', activation='relu', name='conv2D_discriminator_3'))
discriminator.add(keras.layers.MaxPool2D(pool_size=(2, 2), padding='same'))
discriminator.add(keras.layers.Conv2D(32, (3, 3), padding='same', activation='relu', name='conv2D_discriminator_4'))
discriminator.add(keras.layers.MaxPool2D(pool_size=(2, 2), padding='same'))

discriminator.add(keras.layers.Flatten())

discriminator.add(keras.layers.Dropout(dropout_rate))
discriminator.add(keras.layers.Dense(40, activation='relu', name='dense_discriminator_1'))
discriminator.add(keras.layers.Dropout(dropout_rate))
discriminator.add(keras.layers.Dense(10, activation='relu', name='dense_discriminator_2'))
discriminator.add(keras.layers.Dropout(dropout_rate))
discriminator.add(keras.layers.Dense(1, activation='sigmoid', name='dense_discriminator_3'))
discriminator.compile(loss='binary_crossentropy', optimizer=optimizer)
discriminator.summary()


# der Deskriminator wird hier zunächst eingefroren
discriminator.trainable = False

# Dummy Input Vektor
z = keras.layers.Input(shape=(z_size,))

# Durch die Model-Funktion werden Generator und Deskriminiator verkettet
gan_core = discriminator(generator(z))
gan = keras.models.Model(inputs=z, outputs=gan_core)
gan.compile(loss='binary_crossentropy', optimizer=optimizer)
gan.summary()

epochs = 600

batch_count = all_images / batch_size

history_generator = []
history_discriminator = []

for e in range(1, epochs+1):
    print('-'*15, 'Epoche %d' % e, '-'*15)
    for count in tqdm(range(int(batch_count))):

        real_images = real_images_all[np.random.randint(0,
                                                        real_images_all.shape[0], size=batch_size)]

        # Wir definieren die Zufallszahlen für das Bild
        # Anzahl der Zeilen der Zufallszahlen ist batch_size
        # Jede Zufallszahl hat eine Anzahl Spalten wie durch z_size definiert
        z = np.random.normal(0, 1, size=[batch_size, z_size])

        # Aus jeder Zeile der Zufallszahlen entsteht ein Bild im Generator
        fake_images = generator.predict(z, verbose=False)

        # Reale Und Fakebilder zusammenfügen
        x_dis = np.concatenate([real_images, fake_images])

        # Labels festlegen: Reale Bilder=0, Fakebilder=1
        y_dis = np.zeros(2*batch_size)
        y_dis[batch_size:] = 1

        # Diskriminator einschalten und auf dem erzeugten Datensatz fitten
        discriminator.trainable = True
        history_disc = discriminator.fit(
            x_dis, y_dis, verbose=False, batch_size=2*batch_size)
        discriminator.trainable = False
        history_discriminator.append(history_disc.history['loss'])

        # Als nächstes noch den Generator fitten
        # x_gan = np.random.normal(0, 1, size=[batch_size, z_size])
        y_gan = np.zeros(batch_size)
        history_gen = gan.fit(z, y_gan, verbose=False, batch_size=batch_size)
        history_generator.append(history_gen.history['loss'])

    if e == 1 or e == 2 or e == 3 or e == 5 or e % 10 == 0:

        examples = 1
        dim = (1, 1)
        figsize = (14, 14)
        noise = np.random.normal(0, 1, size=[examples, z_size])
        generated_images = generator.predict(noise, verbose=False)
        generated_images = ((generated_images)*255).astype(int)

        count_img= count_img + 1
        # Demobilder anzeigen
        plt.figure(figsize=figsize)
        for i in range(generated_images.shape[0]):
            plt.subplot(dim[0], dim[1], i+1)
            plt.imshow(generated_images[i],
                       interpolation='nearest', cmap='gray_r')
            plt.axis('off')
        plt.tight_layout()
        view = "image_" + str(count_img) + "_.png"
        plt.savefig(view)
        plt.show()

        plt.plot(history_discriminator, label='Discriminator loss')
        plt.plot(history_generator, label='Generator loss')
        plt.legend()
        plt.show()

        #################################################
        # Nächster Abschnitt geht nur wenn z_size=2
        '''
        examples = 121

        # Anstele von Zufallszahlen kontinuierlichen Bereich von -2 bis 2
        # in 2 Dimensionen
        z_size = 2
        noise = np.mgrid[-2:2.1:0.4, -2:2.1:0.4].reshape(2, -1).T

        generated_images = generator.predict(noise)
        generated_images = ((generated_images)*255).astype(int)

        dim = (11, 11)

        plt.figure(figsize=dim)
        for i in range(generated_images.shape[0]):
            plt.subplot(dim[0], dim[1], i+1)
            plt.imshow(generated_images[i], cmap='gray_r')
            plt.axis('off')
        plt.tight_layout()
        plt.show()
        '''
        
# Save Generator and discriminator
discriminator.save("discriminatorTrained.h5")
generator.save("generatorTrained.h5")

# # Load Generator and discriminator
# discriminator = load_model('discriminatorTrained.h5')
# generator = load_model('generatorTrained.h5')
# discriminator.trainable = False

# #Make new GAN from trained discriminator and generator
# gan_input = Input(shape=(noise_dim,))
# fake_image = generator(gan_input)
# gan_output = discriminator(fake_image)
# gan = Model(gan_input, gan_output)
# gan.compile(loss='binary_crossentropy', optimizer=optimizer)