# Generation of atomic distributions with GANs

We use generative adversarial networks (GANs) to create an "artificial" distribution of atoms. Real data was obtained from a combination of quantum mechanical and physical statistics methods. 

## Abstract

We use generative adversarial networks (GANs) to create an "artificial" distribution of atoms. Real data was obtained from a combination of quantum mechanical and physical statistics methods. For this, we designed two different GANs models. One  GANs model mimics images from 2D sets of atomic distributions. In a second model, we provide to the generator and discriminator the x,y, and z atomic coordinates, and let the model synthesize the artificial 3D distribution.

## First results

The generator and discriminator are the two main parts of the code. The generator tries to create images that resemble as much as possible the real images, while the discriminator tries to identify whether the produced images by the generator are fake, hence they coined "Adversarial". The first problem we spotted in our proto-modeling is that the generator had a very high loss function. We partially solved this problem by increasing the number of perceptron layers within the generator. 

Another two factors we took into consideration were the amount of provided images and their resolution. We tested sets of 64x64, 256x256, and 512x512 pixels with a total set of 50, 102, 204, and 408.

Our first attempts show that our GANs model first tries to capture the long-range order. As the number of epochs increases, the GANs struggle to reconcile simultaneously the characteristic long and shor range ordering We notice that the number of Dense layers added to Conv2DTranspose layers,  improved the quality of images and the ordering, the layer configuration. Please note that the number of Conv2DTranspose layers should be adjusted when the image definition of the pixels is increased. The series of images follow the production of the GANs model with an architecture of 64-512-1024-2014 (Generated Images I) and 128-256-512-1024-1024 (Generated Images II) 

As a next step, a more careful architecture for the neural network should be designed.
